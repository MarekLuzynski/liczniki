import sqlite3

def create_table():
    con = sqlite3.connect('test.db')
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    cur.execute("""
    CREATE TABLE IF NOT EXISTS wpisy (
    id INTEGER PRIMARY KEY ASC,
    adres varchar(20) NOT NULL,
    numer varchar(3) NOT NULL,
    E integer NOT NULL,
    U integer NOT NULL,
    S integer NOT NULL
    )""")

    con.commit()
    con.close()


def create_registration(id, street, nr, e, u, s):
    con = sqlite3.connect('test.db')
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    cur.execute("INSERT INTO wpisy VALUES (" + id + ", '" + street + "', '" + nr + "', " + e + ", " + u + ", " + s + ")")
    con.commit()
    con.close()


def read_registrations():
    con = sqlite3.connect('test.db')
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    cur.execute("""
    SELECT id, adres, numer, E, U, S FROM wpisy
    """)

    wpisy_lista = cur.fetchall()

    for wpis in wpisy_lista:
        print("id:" + str(wpis['id']) + " |", str(wpis['adres']), str(wpis['numer']), "| E:" + str(wpis['E']), "| U:" + str(wpis['U']), "| S:" + str(wpis['S']))
    con.close()


# def del_registration(x):
#     con = sqlite3.connect('test.db')
#     con.row_factory = sqlite3.Row
#     cur = con.cursor()
#
#     cur.execute(f'DELETE FROM wpisy WHERE id={str(x)}')
#
#     con.commit()
#     con.close()
