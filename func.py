import os
import sys
import datetime
from time import sleep
import colorama
from colorama import Fore
import operator
from sqlite3_helper import *

colorama.init(autoreset=True)
list_of_reg = []

def logo_printer():
    print(Fore.RED + """
            /\ \       /\ \         /\ \ 
           /  \ \     /  \ \       /  \ \ 
          / /\ \ \   / /\ \ \     / /\ \ \ 
         / / /\ \_\ / / /\ \_\   / / /\ \ \ 
        / / /_/ / // /_/_ \/_/  / / /  \ \_\ 
       / / /__\/ // /____/\    / / /    \/_/ 
      / / /_____// /\____\/   / / / 
     / / /      / / /______  / / /________ 
    / / /      / / /_______\/ / /_________\ 
    \/_/       \/__________/\/____________/  """)

    print(Fore.BLUE + """
    .__  .__                     .__ __   .__ 
    |  | |__| ____ ________ ____ |__|  | _|__|
    |  | |  |/ ___\ \__   //    \|  |  |/ /  |
    |  |_|  \  \___ /    /|   |  \  |    <|  |
    |____/__|\___  >_____ \___|  /__|__|_ \__|
                 \/      \/    \/        \/   
    """)

    animate(Fore.GREEN + "------------Witaj w aplikacji PEC liczniki----------------ver.1.1----------")
    clear_screen_after_sleep(1)

def animate(text, time=0.01):
    for letter in text:
        print(letter, end="")
        sys.stdout.flush()
        sleep(time)

def clear_screen_after_sleep(sleep_time):
    sleep(sleep_time)
    os.system('cls')

def printing_menu():
    print(Fore.WHITE + """\n======================MENU=======================
1. Wprowadź liczniki z danej lokalizacji
2. Usuń konkretny wpis
3. Wyświetl wszystkie wpisy
4. Dodaj adres
5. Usuń adres
6. Pokaż dostępne adresy
7. Zapisz do pliku i zamknij program
""")
    try:
        choice = int(input())

        if choice == 1:
            clear_screen_after_sleep(1)
            new_registration_extended()
        elif choice == 2:
            del_registration()
        elif choice == 3:
            clear_screen_after_sleep(1)
            list_of_registrations()
        elif choice == 4:
            clear_screen_after_sleep(1)
            add_new_address()
        elif choice == 5:
            del_address()
        elif choice == 6:
            clear_screen_after_sleep(1)
            show_address()
        elif choice == 7:
            clear_screen_after_sleep(1)
            save_list_and_quit()
        else:
            clear_screen_after_sleep()
            animate("Coś poszło nie tak. Spróbuj ponownie.\n")
            printing_menu()
    except ValueError:
        animate("Aby dokonać wyboru wpisz liczbę.")
        printing_menu()

def new_registration_extended():
    try:
        list = open("addresses.txt").read().splitlines()

        print("0. Powrót")
        for counter, value in enumerate(list, 1):
            print(str(counter) + ".", value)

        choice_street = int(input())
        street = ""
        id = "NULL"

        if choice_street > 0:
            street = str(list[choice_street - 1])
            nr = input("Podaj numer budynku:")
            e = input("Podaj stan licznika energii elektrycznej:")
            u = input("Podaj stan licznika uzupełniania:")
            s = input("Podaj stan licznika socjalnego:")

            create_table()
            create_registration(id, street, nr, e, u, s)
            animate("Wpis dodano pomyślnie.")

            # print("\nDodano do spisu liczników wpis:\n" + str(registration))
            # clean_in_list()

        elif choice_street == 0:
            printing_menu()
    except IndexError:
        clear_screen_after_sleep(1)
        animate("Wybrałeś zbyt dużą liczbę. Spróbuj ponownie.")
    except FileNotFoundError:
        clear_screen_after_sleep(1)
        animate("Prawdopodobnie plik z adresami nie istnieje.\nDodaj adresy w menu głównym.")
    except ValueError:
        clear_screen_after_sleep(1)
        animate("Aby dokonać wyboru wpisz liczbę.")
    printing_menu()

def del_registration():
    read_registrations()
    choice = int(input("Wprowadź id wpisu do usunięcia: "))

    con = sqlite3.connect('test.db')
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    cur.execute(f'DELETE FROM wpisy WHERE id={str(choice)}')

    con.commit()
    con.close()

    animate(Fore.RED + "Wpis usunięto pomyślnie.")
    printing_menu()

def list_of_registrations():
    read_registrations()
    # if len(list_of_reg) > 0:
    #     clean_in_list()
    #     for registration in list_of_reg:
    #         print(registration)
    # else:
    #     print("Lista jest pusta!")
    printing_menu()

def add_new_address():
    new_address = str(input("Wprowadź nową ulicę: \n"))
    if len(new_address) >= 3:
        with open("addresses.txt", "a") as file:
            file.write(str(new_address.title()) + "\n")
        clean_in_txt()
        animate(f"Dodano pomyślnie adres: {new_address.title()}")
    elif len(new_address) < 3:
        animate("Wprowadzono nieprawidłową nazwę.\nNie dodano adresu.")
    printing_menu()

def del_address():
    try:
        with open('addresses.txt') as file_to_check:
            check = file_to_check.read(1)
            if not check:
                animate('Lista jest pusta. Dodaj adresy.')
            else:
                list_of_addresses = open("addresses.txt").read().splitlines()
                print("0. Powrót")
                for counter, value in enumerate(list_of_addresses, 1):
                    print(str(counter) + ".", value)

                choice_street = int(input())
                street = ""
                if choice_street > 0:
                    deleted = list_of_addresses.pop(choice_street - 1)
                    with open("addresses.txt", "w") as file:
                        for element in list_of_addresses:
                            file.write(str(element) + "\n")
                        clean_in_txt()
                    animate(f"Usunięto ulicę: '{deleted}' z listy dostępnych.")
                    printing_menu()

                elif choice_street == 0:
                    printing_menu()
    except IndexError:
        clear_screen_after_sleep(1)
        animate("Wybrałeś zbyt dużą liczbę. Spróbuj ponownie.")
    except FileNotFoundError:
        clear_screen_after_sleep(1)
        animate("Prawdopodobnie plik z adresami nie istnieje.\nDodaj adresy w menu głównym.")
    except ValueError:
        clear_screen_after_sleep(1)
        animate("Aby dokonać wyboru wpisz liczbę.")
    printing_menu()

def show_address():
    try:
        with open('addresses.txt') as file_to_check:
            check = file_to_check.read(1)
            if not check:
                clear_screen_after_sleep(1)
                animate('Lista jest pusta. Dodaj adresy.')
            else:
                clean_in_txt()
                with open('addresses.txt') as plik:
                    for linia in plik:
                        print(linia.strip())
    except FileNotFoundError:
        clear_screen_after_sleep(1)
        animate("Prawdopodobnie plik z adresami nie istnieje.\nDodaj adresy w menu głównym.")
    printing_menu()

def save_list_and_quit():
    now = datetime.datetime.now()
    date = str(now.year) + "-" + str(now.month) + "-" + str(now.day) + "_" + str(now.hour) + \
        "-" + str(now.minute) + "-" + str(now.second)
    short_date = str(now.year) + "-" + str(now.month) + "-" + str(now.day)

    file_name = input("Wpisz nazwę pliku, np 'miesiąc': ")

    con = sqlite3.connect('test.db')
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    cur.execute("""
        SELECT id, adres, numer, E, U, S FROM wpisy
        """)

    wpisy_lista = cur.fetchall()

    file = open(f"liczniki_{file_name}.txt", "w")
    file.write(f"Lista liczników z dnia: {short_date}\n")
    file.write("E - licznik energii elektrycznej\nU - licznik uzupełniania\nS - licznik socjalny\n")
    file.write("-------------------------------------------------\n")

    for wpis in wpisy_lista:
        file.write(str(wpis['adres']) + " " + str(wpis['numer']) + " " + "E: " + str(wpis['E']) + " " + "U: " + str(wpis['U']) + " " + "S: " + str(wpis['S']) + "\n")
        file.close()
        clear_screen_after_sleep(1)
        print("Twoja lista została zapisana do pliku.\nDo zobaczenia kolejnym razem! =)")

        print("Autor: Marek Łużyński")
        quit()

def clean_in_txt():
    list = open("addresses.txt").read().splitlines()
    sorted_list = sorted(list)
    with open("addresses.txt", "w") as file:
        for element in sorted_list:
            file.write(element + "\n")

def clean_in_list():
    global list_of_reg
    list_of_reg[:] = sorted(list_of_reg, key=operator.attrgetter('street'))

