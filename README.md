# Liczniki
> Project include simple, short Python app for input and save to 'txt' file monthly meter readings

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Screenshots](#screenshots)
* [Setup](#setup)
* [Usage](#usage)
* [Project Status](#project-status)
* [Contact](#contact)

## General Information
Project include simple, short Python app for input and save to 'txt' file monthly meter readings
(objets, lists, sorting list of objects by atributes [key=operator.attrgetter('object atribut')],
files, strings, functions, exceptions, no databases)
The app was develped for help at my work.

## Technologies Used
- Python 3.8.1
- sqlite3

## Screenshots
![Example screenshot](./img/screenshot.png)

## Setup
Program starting at main.py with function menu()

## Usage
Just make your choose in main menu.

## Project Status
Project is: _complete_ and _no longer being worked on_.

## Contact
Created by [@Marecki](https://www.linkedin.com/marekluzynski) - feel free to contact me!

